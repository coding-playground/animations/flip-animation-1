class ArraySorting {
    static sortByAge(inputArray) {
        inputArray.sort((a, b) => {
            if (a[2] > b[2]) {
                return 1;
            }
            if (a[2] < b[2]) {
                return -1;
            }

            return 0;
        });

        return inputArray;
    }

    static sortByName(inputArray) {
        inputArray.sort((a, b) => {
            const nameA = a[1].toUpperCase();
            const nameB = b[1].toUpperCase();
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }

            return 0;
        });

        return inputArray;
    }

    static shuffle(inputArray) {
        const shuffledArray = inputArray;
        let counter = shuffledArray.length;

        while (counter > 0) {
            const index = Math.floor(Math.random() * counter);

            counter -= 1;

            const tempElement = shuffledArray[counter];
            shuffledArray[counter] = shuffledArray[index];
            shuffledArray[index] = tempElement;
        }
        return shuffledArray;
    }
}

export default ArraySorting;
