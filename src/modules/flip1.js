import ArraySorting from './arraySorting';

class Flip1 {
    constructor() {
        this.elementArray = [];
        this.positionArray = [];
        this.oldStates = [];
        this.newStates = [];
        this.initialize();
        this.setListeners();
    }

    initialize() {
        this.personList = document.getElementById('person__list');
        const persons = document.querySelectorAll('.person');

        persons.forEach((person, index) => {
            this.elementArray.push(person);
            this.positionArray.push([index, person.childNodes[0].innerHTML, person.childNodes[1].innerHTML]);
        });
    }

    setListeners() {
        const buttons = document.querySelectorAll('.button');

        buttons.forEach((button) => {
            button.addEventListener('click', this.buttonClicked.bind(this));
        });
    }

    buttonClicked(event) {
        this.oldStates = this.getElementPositions();

        switch (event.target.getAttribute('data-sorting')) {
        case 'age':
            this.positionArray = ArraySorting.sortByAge(this.positionArray);
            break;

        case 'name':
            this.positionArray = ArraySorting.sortByName(this.positionArray);
            break;

        case 'shuffle':
            this.positionArray = ArraySorting.shuffle(this.positionArray);
            break;

        default:
            break;
        }


        this.positionArray.forEach((position) => {
            this.personList.append(this.elementArray[position[0]]);
        });

        this.newStates = this.getElementPositions();

        // calculate inverse
        this.elementArray.forEach((element, index) => {
            element.classList.remove('animating__flip');

            const invertedTop = this.oldStates[index].top - this.newStates[index].top;
            const invertedLeft = this.oldStates[index].left - this.newStates[index].left;

            element.style.transform = `translate(${invertedLeft}px, ${invertedTop}px)`;
        });

        const animationTimeout = setTimeout(() => {
            this.elementArray.forEach((element, index) => {
                element.classList.add('is-animating');
                element.style.transform = '';
                element.addEventListener('transitionend', Flip1.animationCallback);
            });
        }, 10);
    }

    getElementPositions() {
        const retrievedStates = [];

        this.elementArray.forEach((listItem) => {
            retrievedStates.push(listItem.getBoundingClientRect());
        });

        return retrievedStates;
    }

    static animationCallback(event) {
        const target = event.target;

        target.classList.remove('is-animating');
        if (target.style.length === 0) {
            target.removeAttribute('style');
        }
        target.removeEventListener('transitionend', Flip1.animationCallback);
    }
}

export default Flip1;
